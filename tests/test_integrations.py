import os
import pytest
from app import app
from flask import Flask, redirect, url_for, render_template, request
import requests


@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['SERVER_NAME'] = 'TEST'
    client = app.test_client()
    with app.app_context():
        pass
    app.app_context().push()
    yield client

def test_index(client):
    rv = client.get('/')
    reponse = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    assert rv.status_code == 200
    assert rv.data != None
    assert reponse.status_code == 200
    
def test_detail(client):
    rv = client.get('/dinosaur')
    assert rv.status_code == 302
    rv =client.get('/dinosaur/brachiosaurus')
    reponse = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/brachiosaurus')
    assert rv.status_code == 200
    assert rv.data != None
